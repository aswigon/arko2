;
; Copyright (c) 2017 Artur J. Świgoń <aswigon@aswigon.pl>
;
; void lissajous(unsigned char *buffer, int width, int height, int stride,
;    double alpha, double beta, double delta, double range, double step);
;
; Reference implementation:
;{
;    double t;
;
;    for (t = -range; t <= range; t += step)
;    {
;        double dx, dy;
;        int ix, iy;
;
;        dx = ((width / 2) - 16) * sin(alpha * t + delta);
;        dy = ((height / 2) - 16) * sin(beta * t);
;
;        ix = (int)dx + (width/2);
;        iy = (int)dy + (height/2);
;
;        buffer[(iy*stride)+ix] = 0xFF;
;    }
;
;    return;
;}
;
; x86_64 calling convention:
; Integer (and pointer) arguments: rdi, rsi, rdx, rcx, r8, r9
; Floating point arguments: xmm[0..7]
; Return value: rax
; Floating point return value: xmm0
;

    global lissajous
    extern sin
    section .text

lissajous:
    ; enter
    push rbp
    mov rbp, rsp

    ; push buffer address [rbp - 8]
    push rdi
    ; push stride [rbp - 16]
    push rcx
    ; push width/2 [rbp - 24]
    sar rsi, 1
    push rsi
    ; push height/2 [rbp - 32]
    sar rdx, 1
    push rdx

    ; free slot [rbp - 40]

    ; advance stack pointer (16-byte alignment)
    ; extra space for sin() result
    sub rsp, 96

    ; pack1 = [alpha; beta] [rbp - 48]
    shufpd xmm0, xmm1, 0
    movapd [rbp - 48], xmm0

    ; pack2 = [delta; zero] [rbp - 64]
    xor r10, r10
    cvtsi2sd xmm1, r10
    shufpd xmm2, xmm1, 0
    movapd [rbp - 64], xmm2

    ; pack3 = [(width/2)-16; (height/2)-16] [rbp - 80]
    sub rsi, 16
    cvtsi2sd xmm0, rsi
    sub rdx, 16
    cvtsi2sd xmm1, rdx
    shufpd xmm0, xmm1, 0
    movapd [rbp - 80], xmm0

    ; push range [rbp - 96]
    movlpd [rbp - 96], xmm3
    ; push step [rbp - 104]
    movlpd [rbp - 104], xmm4

    ; t = -range
    xorpd xmm5, xmm5
    subsd xmm5, xmm3
    ; push t
    movlpd [rbp - 112], xmm5

.draw_loop_entry:
    ; get [alpha; beta]
    movapd xmm0, [rbp - 48]
    ; get t and make [t; t];
    ; (movddup is SSE3)
    movddup xmm7, [rbp - 112]
    ; multiply by [t; t]
    mulpd xmm0, xmm7
    ; add [delta, zero]
    addpd xmm0, [rbp - 64]

    ; store lower argument
    movlpd [rbp - 120], xmm0
    ; move high to low
    shufpd xmm0, xmm0, 1
    ; sin()
    call sin
    ; store sin() result
    movlpd [rbp - 128], xmm0
    ; retrieve lower argument
    movlpd xmm0, [rbp - 120]
    ; sin()
    call sin

    ; pack [sin(x); sin(y)]
    movhpd xmm0, [rbp - 128]
    ; multiply by [(width/2)-16; (height/2)-16]
    mulpd xmm0, [rbp - 80]
    ; convert to int
    cvtsd2si r8, xmm0
    ; move high to low
    shufpd xmm0, xmm0, 1
    ; convert to int
    cvtsd2si r9, xmm0
    ; add [width/2; height/2]
    add r8, [rbp - 24]
    add r9, [rbp - 32]

    ; y *= stride
    imul r9, [rbp - 16]
    ; add x
    add r9, r8
    ; add buffer address
    add r9, [rbp - 8]

    ; store byte
    mov byte [r9], 0xFF

.draw_loop_condition:
    ; get t
    movlpd xmm5, [rbp - 112]
    ; add step
    addsd xmm5, [rbp - 104]
    ; update t
    movlpd [rbp - 112], xmm5
    ; compare with range
    comisd xmm5, [rbp - 96]
    jna .draw_loop_entry

    ; return
    mov rsp, rbp
    pop rbp
    ret
