#
# Copyright (c) 2017 Artur J. Świgoń <aswigon@aswigon.pl>
#

CC=clang
AS=nasm

CPPFLAGS=-DNDEBUG $$(pkg-config --cflags gtk+-2.0)
CFLAGS=-O2 -g0 -Wall -Wextra -std=c99 -pedantic -pthread -m64
ASFLAGS=-f macho64
LDFLAGS=-Wl,-dead_strip_dylibs $$(pkg-config --libs gtk+-2.0)

exename=arko2
objects=lissajous.o window.o

all: $(exename)

$(exename): $(objects)
	$(CC) $(CFLAGS) $(objects) -o $(exename) $(LDFLAGS)

lissajous.o: lissajous.s
	$(AS) $(ASFLAGS) lissajous.s

window.o: window.c lissajous.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c window.c

clean:
	rm -f $(exename)
	rm -f $(objects)
