/*
 *  Copyright (c) 2017 Artur J. Świgoń <aswigon@aswigon.pl>
 */

#include <cairo.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lissajous.h"

#define AUTHOR "Artur J. Świgoń"
#define UI_PAD 6
#define IMG_DIM 512

#define DFLT_A 4.0
#define DFLT_B 5.0
#define DFLT_D 0.0
#define DFLT_N 1000.0
#define DFLT_R 4.0

/* Global data */
struct
{
    unsigned char *buffer;
    double alpha;
    double beta;
    double delta;
    double range;
    double step;
    GtkWidget *darea;
}
global;

/* "delete-event" stub */
static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
    (void)widget;
    (void)event;
    (void)data;
    return FALSE;
}

/* Quit application when its window is closed */
static void destroy(GtkWidget *widget, gpointer data)
{
    (void)widget;
    (void)data;
    gtk_main_quit();
    return;
}

/* "expose-event" is also known as "draw" */
static gboolean expose_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
    (void)event;
    (void)data;

    int width;
    int height;
    int stride;
    cairo_t *cr;
    cairo_surface_t *surface;
    cairo_format_t format = CAIRO_FORMAT_A8;

    width = widget->allocation.width;
    height = widget->allocation.height;

#if 0 /* Unavailable on galera.ii.pw.edu.pl */
    stride = cairo_format_stride_for_width(format, width);
#else
    if (width % 128 == 0)
        stride = width;
    else
        stride = (((width / 128) + 1) * 128);
#endif

    /* Allocate and zero-fill */
    global.buffer = g_malloc0(height * stride);

    lissajous(global.buffer, width, height, stride,
        global.alpha, global.beta, global.delta, global.range, global.step);

#if 0 /* Unavailable on galera.ii.pw.edu.pl */
    cr = gdk_cairo_create(gtk_widget_get_window(widget));
#else
    cr = gdk_cairo_create(widget->window);
#endif

    surface = cairo_image_surface_create_for_data(global.buffer, format,
        width, height, stride);
    cairo_set_source_surface(cr, surface, 0.0, 0.0);
    cairo_paint(cr);

    cairo_surface_destroy(surface);
    cairo_destroy(cr);
    g_free(global.buffer);
    global.buffer = NULL;

    return FALSE;
}

static gboolean change_value_a(GtkRange *range, GtkScrollType scroll,
    gdouble value, gpointer data)
{
    (void)range;
    (void)scroll;
    (void)data;
    global.alpha = round(value * 10.0) / 10.0;
    gtk_widget_queue_draw(global.darea);
    return FALSE;
}

static gboolean change_value_b(GtkRange *range, GtkScrollType scroll,
    gdouble value, gpointer data)
{
    (void)range;
    (void)scroll;
    (void)data;
    global.beta = round(value * 10.0) / 10.0;
    gtk_widget_queue_draw(global.darea);
    return FALSE;
}

static gboolean change_value_d(GtkRange *range, GtkScrollType scroll,
    gdouble value, gpointer data)
{
    (void)range;
    (void)scroll;
    (void)data;
    global.delta = round(value * 100.0) / 100.0;
    gtk_widget_queue_draw(global.darea);
    return FALSE;
}

static gboolean change_value_n(GtkRange *range, GtkScrollType scroll,
    gdouble value, gpointer data)
{
    (void)range;
    (void)scroll;
    (void)data;
    /* Step is a reciprocal of number of points */
    global.step = (1.0 / value);
    gtk_widget_queue_draw(global.darea);
    return FALSE;
}

static gboolean change_value_r(GtkRange *range, GtkScrollType scroll,
    gdouble value, gpointer data)
{
    (void)range;
    (void)scroll;
    (void)data;
    global.range = round(value * 10.0) / 10.0;
    gtk_widget_queue_draw(global.darea);
    return FALSE;
}

int main(int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *hbox;
    GtkWidget *vpanel;
    GtkWidget *formula;
    GtkWidget *author;

    GtkWidget *label_a, *label_b, *label_d, *label_n, *label_r;
    GtkWidget *scale_a, *scale_b, *scale_d, *scale_n, *scale_r;

    /* Initialize GTK+ */
    gtk_init(&argc, &argv);

    /* Initialize global data */
    global.alpha = DFLT_A;
    global.beta = DFLT_B;
    global.delta = DFLT_D;
    global.range = DFLT_R;
    global.step = (1 / DFLT_N);

    /* Create main window */
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "[ARKO] Lissajous");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "delete-event", G_CALLBACK(delete_event), NULL);
    g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);

    /* Create a horizontal box as the main container */
    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), hbox);

    /* Create a vertical panel on the right hand side */
    vpanel = gtk_vbox_new(FALSE, 0);
    gtk_widget_set_size_request(vpanel, 192, IMG_DIM);
    gtk_box_pack_end(GTK_BOX(hbox), vpanel, FALSE, FALSE, UI_PAD);

    /* Create the drawing area */
    global.darea = gtk_drawing_area_new();
    gtk_widget_set_size_request(global.darea, IMG_DIM, IMG_DIM);
    gtk_box_pack_start(GTK_BOX(hbox), global.darea, TRUE, TRUE, UI_PAD);
    g_signal_connect(global.darea, "expose-event",
        G_CALLBACK(expose_event), NULL);

    formula = gtk_label_new("x(t) = w × sin(αt + δ)\n"
        "y(t) = h × sin(βt)\n"
        "t ∈ [–r, r]");
    gtk_box_pack_start(GTK_BOX(vpanel), formula, FALSE, FALSE, UI_PAD);

    /* Scale for parameter alpha */
    scale_a = gtk_hscale_new_with_range(1.0, 10.0, 0.1);
    gtk_range_set_value(GTK_RANGE(scale_a), DFLT_A);
    gtk_box_pack_start(GTK_BOX(vpanel), scale_a, FALSE, FALSE, UI_PAD);
    g_signal_connect(scale_a, "change-value",
        G_CALLBACK(change_value_a), NULL);
    label_a = gtk_label_new("α");
    gtk_box_pack_start(GTK_BOX(vpanel), label_a, FALSE, FALSE, 0);

    /* Scale for parameter beta */
    scale_b = gtk_hscale_new_with_range(1.0, 10.0, 0.1);
    gtk_range_set_value(GTK_RANGE(scale_b), DFLT_B);
    gtk_box_pack_start(GTK_BOX(vpanel), scale_b, FALSE, FALSE, UI_PAD);
    g_signal_connect(scale_b, "change-value",
        G_CALLBACK(change_value_b), NULL);
    label_b = gtk_label_new("β");
    gtk_box_pack_start(GTK_BOX(vpanel), label_b, FALSE, FALSE, 0);

    /* Scale for parameter delta */
    scale_d = gtk_hscale_new_with_range(0.0, 6.283185, 0.01);
    gtk_range_set_value(GTK_RANGE(scale_d), DFLT_D);
    gtk_box_pack_start(GTK_BOX(vpanel), scale_d, FALSE, FALSE, UI_PAD);
    g_signal_connect(scale_d, "change-value",
        G_CALLBACK(change_value_d), NULL);
    label_d = gtk_label_new("δ");
    gtk_box_pack_start(GTK_BOX(vpanel), label_d, FALSE, FALSE, 0);

    /* Scale for parameter 1/dt */
    scale_n = gtk_hscale_new_with_range(500.0, 5000.0, 100.0);
    gtk_range_set_value(GTK_RANGE(scale_n), DFLT_N);
    gtk_box_pack_start(GTK_BOX(vpanel), scale_n, FALSE, FALSE, UI_PAD);
    g_signal_connect(scale_n, "change-value",
        G_CALLBACK(change_value_n), NULL);
    label_n = gtk_label_new("1/dt");
    gtk_box_pack_start(GTK_BOX(vpanel), label_n, FALSE, FALSE, 0);

    /* Scale for parameter range */
    scale_r = gtk_hscale_new_with_range(1.0, 10.0, 0.1);
    gtk_range_set_value(GTK_RANGE(scale_r), DFLT_R);
    gtk_box_pack_start(GTK_BOX(vpanel), scale_r, FALSE, FALSE, UI_PAD);
    g_signal_connect(scale_r, "change-value",
        G_CALLBACK(change_value_r), NULL);
    label_r = gtk_label_new("r");
    gtk_box_pack_start(GTK_BOX(vpanel), label_r, FALSE, FALSE, 0);

    author = gtk_label_new(AUTHOR);
    gtk_box_pack_end(GTK_BOX(vpanel), author, FALSE, FALSE, UI_PAD);

    gtk_widget_show_all(window);

    /* Run main loop */
    gtk_main();

    return 0;
}
