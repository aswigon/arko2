/*
 *  Copyright (c) 2017 Artur J. Świgoń <aswigon@aswigon.pl>
 */

#ifndef LISSAJOUS_H
#define LISSAJOUS_H

#ifdef __cplusplus
extern "C" {
#endif

void lissajous(unsigned char *buffer, int width, int height, int stride,
    double alpha, double beta, double delta, double range, double step);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* !LISSAJOUS_H */
